---
layout: home

hero:
  name: General IQRF gateway IQD-GW02 (IQUBE)
  text: documentation
  tagline: 'Ready-to-use IQRF gateway with Ethernet connection.'
  image:
    light: /images/iqd-gw-01_original.png
    dark: /images/iqd-gw-01_original-dark.png
    alt: IQRF gateway IQD-GW02 (IQUBE)
  actions:
    - theme: brand
      text: Get Started
      link: /introduction.html
    - theme: alt
      text: Product page
      link: https://www.iqrf.org/product-detail/iqd-gw-02-iqube
    - theme: alt
      text: E-shop
      link: https://eshop.iqrf.org/p/iqube
---
