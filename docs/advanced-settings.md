# Advanced settings

## Webapp's User password

In case of **forgotten user password** it is possible to change the password to
already existing user via console utility.

![Console utility](/images/ssh-passchange-page.png)

Read more about webapp CLI
[here](https://docs.iqrf.org/iqrf-gateway/user/webapp/cli.html).
