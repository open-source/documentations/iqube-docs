# News

## Summary

* Image v1.7.0 (Armbian Bookworm and Yocto Kirkstone)
  * Daemon: Release v2.6.x
  * Daemon: New IQRF standard Light supported
  * Daemon: IQRF standard DALI deprecated
  * Daemon: Standard FRCs reworked to include also MID and HWPID in JSON responses
  * Daemon: Set logging level of deployment configurations to warning
  * Daemon: Autonetwork service always reports new nodes
  * Daemon: IDE counterpart includes GW_ADDR in asynchronous TR packets
  * Daemon: Mqtt messaging sets cleanSession according to QOS value
  * Daemon: Detailed [changes and fixes](https://gitlab.iqrf.org/open-source/iqrf-gateway-daemon/-/blob/release/v2.6.x/debian/changelog)
  * Webapp: Release v2.6.x
  * Webapp: New IQRF standard Light supported
  * Webapp: IQRF standard DALI components removed
  * Webapp: Mender client configuration fix in Maintenance
  * Webapp: Loading JSON schemas from the filesystem
  * Webapp: Added Syslog logging configuration
  * Webapp: MQTT configuration uses autodetection for TLS usage
  * Webapp: Removed Inteliments InteliGlue MQTT connection creator from Cloud services
  * Webapp: Detailed [changes and fixes](https://gitlab.iqrf.org/open-source/iqrf-gateway-webapp/-/blob/release/v2.6.x/debian/changelog)
  * Setter: HWPID added in Gateway configuration file
  * Cloud: HWPID added in MQTT topics path configuration
  * Image: Armbian Bookworm build v24.08
  * Image: Yocto Kirkstone build
  * IQRF Gitlab tag: v25.02.0

-- Rostislav Spinar rostislav.spinar@iqrf.com Mon, 03 Mar 2025 20:00:00 +0100

* Image v1.6.0 (Armbian Bullseye/Bookworm and Yocto Kirkstone)
  * Daemon: Release v2.5.x
  * Daemon: DALI command included in FRC response
  * Daemon: Standard FRC reworked with little fixes
  * Daemon: IQRF repository cache fix for DALI standard
  * Daemon: Default FRC timeout changed from infinite to 2 minutes
  * Webapp: Release v2.5.x
  * Webapp: Password change for root/admin users enabled in initial setup wizard and SSH service page
  * Webapp: Mender client v4 supported
  * Webapp: Backup manager improved
  * Webapp: Spinner added for reboot and poweroff actions
  * Cloud: IQRF cloud system supported
  * Image: Armbian build v23.11
  * Image: Default Armbian SSH root password set to gateway ID
  * Image: Default Yocto SSH admin password set to gateway ID
  * IQRF Gitlab tag: v24.04.0

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Mon, 13 May 2024 12:35:00 +0200

* Image v1.5.1 (Armbian Bullseye and Yocto Kirkstone)
  * Daemon: Release v2.5.x
  * Daemon: IQRF-UDP protocol reworked and completed
  * Daemon: [IQRFPY](https://apidocs.iqrf.org/iqrfpy/latest/iqrfpy.html) library support
  * Webapp: Release v2.5.x
  * Webapp: Monit system checks setting enabled
  * Webapp: System services menu created
  * Webapp: Tuptime log added into gateway diagnostic logs
  * Image: Armbian build v23.11
  * Image: Node-RED programming tool added
  * IQRF Gitlab tag: v24.01.0

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Mon, 29 Jan 2024 11:30:00 +0100

* Image v1.5.0 (Armbian Bullseye and Yocto Kirkstone)
  * Daemon: Release v2.4.x
  * Deamon: Runs after gateway time-synchronization is done or after 2 m timeout
  * Daemon: Address space allocation and MID list extensions for AutoNetwork
  * Daemon: DPA params and Maintenance IQMESH services supported
  * Daemon: Lite DB supported and accessible via IQRF Info API
  * Daemon: Scheduler reworked, support for a task enabling and disabling
  * Daemon: IQRF Repository cache update support
  * Webapp: Release v2.5.x
  * Webapp: Bonding NFC reader added
  * Webapp: Address space allocation and MID list support for AutoNetwork
  * Webapp: Standard manager network enumeration with list of devices table
  * Webapp: IP manager ETH, WiFi, GSM reworked
  * Webapp: Log management improved, Journal dynamic loading supported
  * Webapp: Gateway maintenance supported, Backup and restore, Remote monitoring
  * Webapp: Support for KONA-RASP-04 adapter mapping
  * Webapp: Support for APCUPSD service management
  * Webapp: IQRF Repository cache update support
  * Controller: Release v1.4.x, software extended and improved
  * Setter: Release v1.1.x, software extended and improved
  * MQTT broker: secured by default user: `iqrf`, password: `GWID string`
  * APCUPSD manager: Power management for IQRF UPS device added
  * Mender clients: Included and configurable via Webapp UI
  * Pixla client: Removed, not supported any more
  * Onboard WiFi: Disabled by default, recommended to use USB WiFi dongle
  * Power uUSB changed from USB device to USB host mode to support IQRF UPS
  * Linux kernel: 5.15.x-sunxi
  * Armbian LTS Bullseye release
  * Yocto application Docker containers supported
  * Yocto LTS Kirkstone release
  * IQRF Gitlab tags: v23.04.0

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Tue, 17 Apr 2023 10:00:00 +0100

* Image v1.4.6 (Armbian and Yocto)
  * Daemon updates: MqttMessaging connection logic reworked, support for TR-7xG transceiver series in IQMESH services
  * Controller updates: Improved logging, logrotate rules
  * Uploader updates: Support for TR-7xG transceiver series
  * Paho updates: Library has been updated to v1.3.10-1 for Armbian based gateways
  * Armbian updates for LTS Buster release
  * Yocto updates for LTS Dunfell release

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Tue, 23 Aug 2022 20:10:00 +0200

* Image v1.4.4 (Armbian and Yocto)
  * Daemon updates: AutoNetwork, IqrfUart, UdpChannel components improved
  * Webapp improvements: GSM connections setup, WebSocket handling
  * Setter introduced: package release and replacing setter script
  * Armbian feature: Added support for Mender remote management
  * Armbian/Yocto: Pixla service is disabled by default
  * Armbian updates for LTS Buster release
  * Yocto updates for LTS Dunfell release

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Tue, 07 Jun 2022 11:45:00 +0200

* Image v1.4.2 (Armbian and Yocto)
  * Webapp feature: Gateway backup and restore
  * Controller improvement: logrotate on file size
  * Armbian feature: Replace ntp by systemd-timesyncd
  * Yocto fix: Logs kept in memory
  * Yocto fix: Removed systemd-networkd

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Fri, 18 Feb 2022 12:24:00 +0100

* Image v1.4.0 (Armbian and Yocto)
  * Daemon: IQRF write TR extended for broadcast
  * Daemon: IQRF bonding services error handling improved
  * Daemon: IQRF Info component DB reset support added
  * Daemon: IQRF Info component C enumeration improved
  * Webapp: Installation wizard introduced
  * Webapp: User roles introduced
  * Webapp: User email support together with password recovery
  * Webapp: GSM connections support
  * Webapp: IQRF Standard manager improved
  * Webapp: Coordinator indication added
  * Armbian, Yocto: Watchdog service enabled
  * Armbian, Yocto: GPIO watchdog module enabled
  * Armbian, Yocto: External USB WiFi support added

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Wed, 22 Dec 2021 20:00:00 +0100

* Image v1.3.2 (Yocto)
  * Webapp feature: Gateway backup and restore
  * Controller improvement: logrotate on file size
  * Yocto fix: Logs kept in memory
  * Yocto fix: Removed systemd-networkd

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Thu, 24 Feb 2022 13:28:00 +0100

* Image v1.3.1 (Yocto)
  * Webapp: NTP manager added
  * Webapp: IPv4 address of DNS servers fixed
  * Webapp: Journald settings updated
  * Yocto: Onboard Wifi disabled
  * Yocto: Watchdog service added

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Tue, 14 Sep 2021 15:00:00 +0100

* Image v1.3.0 (Armbian and Yocto)
  * Daemon: IQRF AutoNetwork component extended
  * Daemon: IQRF OTA component added
  * Daemon: IQRF UDP component improved
  * Daemon: Syslog component added
  * Daemon: Error pin handling for KON-RASP-01
  * Daemon: Linux process ID management added
  * Daemon: IQRF UART as primary interface
  * Webapp: IP gateway manager \[ETH, WiFi, VPN\] introduced
  * Webapp: IQRF DPA upload for \[C\] added
  * Webapp: UI for IQRF OTA service added
  * Webapp: Maintenance menu \[Pixla, Mender, Monit\] introduced
  * Webapp: Daemon Scheduler UI improved
  * Webapp: JSON message formatter
  * Webapp: Log management improved
  * Webapp: Hostname change added
  * Controller: Linux process ID management added
  * Uploader: IQRF SPI uploader introduced

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Mon, 19 Jul 2021 23:55:00 +0100

* Doc update
  * Including IQD-GW-02x device
  * Reworked Start guide

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Tue, 01 Dec 2020 19:00:00 +0100

* News update
  * Controller doc update
  * Armbian Image releases update
  * Yocto image with Mender OTA

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Fri, 06 Nov 2020 02:00:00 +0100

* Start tutorial
  * Change in hostname

* Update OTA
  * Introduction, link to IQRF GW daemon page

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Wed, 24 Jun 2020 16:00:00 +0100

* Start tutorial
  * Update scheduler configuration, import using zip file

* Application tutorial
  * Adding scheduler task using Standard sensor FRC, both methods Polling x FRC included

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Mon, 11 May 2020 14:00:00 +0100

* Application tutorial
  * Connecting multiple gateways to MQTT broker and reading sensory devices

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Mon, 13 Apr 2020 16:00:00 +0100

## Releases

1.7.0 stable; urgency=medium

\[ Karel Hanak \]
\[ Roman Ondráček \]
\[ Ondřej Hujňák \]
\[ Rostislav Spinar \]

* Armbian image:
  * iqd-gw-02_armbian-bookworm_uart_v170_20250224.img.gz

* Yocto image:
  * iqd-gw-02_yocto-kirkstone_uart_v170_20250224.sdimg.bz2
  * iqd-gw-02_yocto-kirkstone_uart_v170_20250224.mender

* IQRF GW software
  * iqrf-gateway-daemon (2.6.2)
  * iqrf-gateway-webapp (2.6.3)
  * iqrf-gateway-controller (1.5.0)
  * iqrf-gateway-uploader (1.0.7)
  * iqrf-gateway-setter (1.6.0)
  * iqrf-cloud-provisioning (0.1.2)
  * Armbian updates for LTS Bookworm release
  * Yocto updates for LTS Kirkstone release

-- Rostislav Spinar <rostislav.spinar@iqrf.com> Mon, 03 Mar 2024 20:00:00 +0100

1.6.0 stable; urgency=medium

\[ Karel Hanak \]
\[ Roman Ondráček \]
\[ Rostislav Spinar \]

* Armbian image:
  * iqd-gw-02_armbian-bookworm_uart_v160_20240417.img.gz
  * iqd-gw-02_armbian-bullseye_uart_v160_20240417.img.gz

* Yocto image:
  * iqd-gw-02_yocto-kirkstone_uart_v160_20240426.sdimg.bz2
  * iqd-gw-02_yocto-kirkstone_uart_v160_20240426.mender

* IQRF GW software
  * iqrf-gateway-daemon (2.5.7)
  * iqrf-gateway-webapp (2.5.20)
  * iqrf-gateway-controller (1.5.0)
  * iqrf-gateway-uploader (1.0.7)
  * iqrf-gateway-setter (1.3.0)
  * iqrf-cloud-provisioning (0.1.1)
  * Armbian updates for LTS Bookworm/Bullseye release
  * Yocto updates for LTS Kirkstone release

-- Rostislav Spinar <rostislav.spinar@iqrf.com> Mon, 13 May 2024 10:30:00 +0200

1.5.1 stable; urgency=medium

\[ Karel Hanak \]
\[ Roman Ondráček \]
\[ Rostislav Spinar \]

* Armbian image:
  * iqd-gw-02_armbian-bullseye_uart_v151_20240129.img.gz

* Yocto image:
  * iqd-gw-02_yocto-kirkstone_uart_v151_20240119.sdimg.bz2
  * iqd-gw-02_yocto-kirkstone_uart_v151_20240119.mender

* IQRF GW software
  * iqrf-gateway-daemon (2.5.6)
  * iqrf-gateway-webapp (2.5.16)
  * iqrf-gateway-controller (1.5.0)
  * iqrf-gateway-uploader (1.0.7)
  * iqrf-gateway-setter (1.2.2)
  * Armbian updates for LTS Bullseye release
  * Yocto updates for LTS Kirkstone release

-- Rostislav Spinar <rostislav.spinar@iqrf.com> Mon, 29 Jan 2024 11:30:00 +0100

1.5.0 stable; urgency=medium

\[ Karel Hanak \]
\[ Roman Ondráček \]
\[ Vasek Hanak \]
\[ Marek Belisko \]
\[ Rostislav Spinar \]

* Armbian image:
  * iqd-gw-02_armbian-bullseye_uart_v150_20230414.img.gz

* Yocto image:
  * iqd-gw-02_yocto-kirkstone_uart_v150_20230417.sdimg.bz2
  * iqd-gw-02_yocto-kirkstone_uart_v150_20230417.mender

* IQRF GW software
  * iqrf-gateway-daemon (2.4.3)
  * iqrf-gateway-webapp (2.5.6)
  * iqrf-gateway-controller (1.4.2)
  * iqrf-gateway-uploader (1.0.5)
  * iqrf-gateway-setter (1.1.3)

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Mon, 17 Apr 2023 10:30:00 +0100

1.4.6 stable; urgency=medium

\[ Karel Hanak \]
\[ Roman Ondráček \]
\[ Vasek Hanak \]
\[ Marek Belisko \]
\[ Rostislav Spinar \]

* Armbian image:
  * iqd-gw-02_armbian-buster_uart_v146_20220822.img.gz

* Yocto image:
  * iqd-gw-02_yocto-dunfell_uart_v146_20220822.sdimg.bz2
  * iqd-gw-02_yocto-dunfell_uart_v146_20220822.mender

* IQRF GW software
  * iqrf-gateway-daemon (2.3.13)
  * iqrf-gateway-webapp (2.4.11)
  * iqrf-gateway-controller (1.3.6)
  * iqrf-gateway-uploader (1.0.3)
  * iqrf-gateway-setter (1.0.4)

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Tue, 23 Aug 2022 20:00:00 +0200

1.4.4 stable; urgency=medium

\[ Karel Hanak \]
\[ Roman Ondráček \]
\[ Vasek Hanak \]
\[ Marek Belisko \]
\[ Rostislav Spinar \]

* Armbian image:
  * iqd-gw-02_armbian-buster_uart_v144_20220605.img.gz

* Yocto image:
  * iqd-gw-02_yocto-dunfell_uart_v144_20220606.sdimg.bz2
  * iqd-gw-02_yocto-dunfell_uart_v144_20220606.mender

* IQRF GW software
  * iqrf-gateway-daemon (2.3.12)
  * iqrf-gateway-webapp (2.4.8)
  * iqrf-gateway-controller (1.3.5)
  * iqrf-gateway-uploader (1.0.2)
  * iqrf-gateway-setter (1.0.3)

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Tue, 07 Jun 2022 11:30:00 +0200

1.4.2 stable; urgency=medium

\[ Karel Hanak \]
\[ Roman Ondráček \]
\[ Marek Belisko \]
\[ Vasek Hanak \]
\[ Rostislav Spinar \]

* Armbian image:
  * iqd-gw-02_armbian-buster_uart_v142_20220218.img.gz

* Yocto image:
  * iqd-gw-02_yocto-dunfell_uart_v142_20220217.sdimg.bz2
  * iqd-gw-02_yocto-dunfell_uart_v142_20220217.mender

* IQRF GW software
  * iqrf-gateway-daemon (2.3.9)
  * iqrf-gateway-webapp (2.4.2)
  * iqrf-gateway-controller (1.3.2)
  * iqrf-gateway-uploader (1.0.2)

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Fri, 18 Feb 2022 12:24:00 +0100

1.4.0 stable; urgency=medium

\[ Karel Hanak \]
\[ Roman Ondráček \]
\[ Marek Belisko \]
\[ Vasek Hanak \]
\[ Rostislav Spinar \]

* Armbian image:
  * iqd-gw-02_armbian-buster_uart_v140_20211222.img.gz

* Yocto image:
  * iqd-gw-02_yocto-dunfell_uart_v140_20211220.sdimg.bz2
  * iqd-gw-02_yocto-dunfell_uart_v140_20211220.mender

* IQRF GW software
  * iqrf-gateway-daemon (2.3.8)
  * iqrf-gateway-webapp (2.4.0)
  * iqrf-gateway-controller (1.3.1)
  * iqrf-gateway-uploader (1.0.2)

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Wed, 22 Dec 2021 20:00:00 +0100

1.3.2 stable; urgency=medium

\[ Karel Hanak \]
\[ Roman Ondráček \]
\[ Marek Belisko \]
\[ Vasek Hanak \]
\[ Rostislav Spinar \]

* Yocto image:
  * iqd-gw-02_yocto-dunfell_uart_v132_20220224.sdimg.bz2
  * iqd-gw-02_yocto-dunfell_uart_v132_20220224.mender

* IQRF GW software
  * iqrf-gateway-daemon (2.3.9)
  * iqrf-gateway-webapp (2.4.2)
  * iqrf-gateway-controller (1.3.2)
  * iqrf-gateway-uploader (1.0.2)

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Thu, 24 Feb 2022 13:28:00 +0100

1.3.1 stable; urgency=medium

\[ Karel Hanak \]
\[ Roman Ondráček \]
\[ Marek Belisko \]
\[ Rostislav Spinar \]

* Yocto image:
  * iqd-gw-02_yocto-dunfell_uart_v131_20210912.sdimg.bz2
  * iqd-gw-02_yocto-dunfell_uart_v131_20210912.mender

* IQRF GW software
  * iqrf-gateway-daemon (2.3.7)
  * iqrf-gateway-webapp (2.3.3)
  * iqrf-gateway-controller (1.2.1)
  * iqrf-gateway-uploader (1.0.1)

-- Karel HanÃ¡k <karel.hanak@iqrf.com>  Tue, 14 Sep 2021 11:37:00 +0100

1.3.0 stable; urgency=medium

\[ Karel Hanak \]
\[ Roman Ondráček \]
\[ Marek Belisko \]
\[ Vasek Hanak \]
\[ Rostislav Spinar \]

* Armbian image:
  * iqd-gw-02_armbian-buster_uart_v130_20210717.img.gz

* Yocto image:
  * iqd-gw-02_yocto-dunfell_uart_v130_20210718.sdimg.bz2
  * iqd-gw-02_yocto-dunfell_uart_v130_20210718.mender
  * iqd-gw-02_yocto-dunfell-sdk_v130_20210527.sh

* IQRF GW software
  * iqrf-gateway-daemon (2.3.7)
  * iqrf-gateway-webapp (2.3.2)
  * iqrf-gateway-controller (1.2.1)
  * iqrf-gateway-uploader (1.0.1)

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Mon, 19 Jul 2021 23:55:00 +0100

1.2.3 stable; urgency=medium

 \[ Rostislav Spinar \]

* iqrf-gateway-daemon (2.3.5)
* iqrf-gateway-webapp (2.2.8)
* iqrf-gateway-controller (1.1.2)

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Thu, 04 Mar 2021 10:00:00 +0100

1.2.0 stable; urgency=medium

 \[ Rostislav Spinar \]

* iqrf-gateway-daemon (2.3.5)
* iqrf-gateway-webapp (2.2.3)
* iqrf-gateway-controller (1.1.2)

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Fri, 08 Jan 2021 12:19:00 +0100

1.1.1 stable; urgency=medium

 \[ Roman Ondráček \]
 \[ Rostislav Spinar \]

* iqrf-gateway-daemon (2.2.0)
* iqrf-gateway-webapp (2.0.2)
* iqrf-gateway-controller (1.0.0)
* wifi ap support

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Wed, 09 Sep 2020 7:00:00 +0200

1.1.0 stable; urgency=medium

 \[ Roman Ondráček \]
 \[ Rostislav Spinar \]

* iqrf-gateway-daemon (2.2.0)
* iqrf-gateway-webapp (2.0.2)
* iqrf-gateway-controller (1.0.0)
* lte usb dongle support

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Mon, 07 Sep 2020 7:00:00 +0200

1.0.0 stable; urgency=medium

 \[ Roman Ondráček \]
 \[ Rostislav Spinar \]

* iqrf-gateway-daemon (2.2.0)
* iqrf-gateway-webapp (2.0.0)
* iqrf-gateway-controller (1.0.0)

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Wed, 24 Jun 2020 12:00:00 +0200

1.0.0-rc6 testing; urgency=medium

 \[ Roman Ondráček \]
 \[ Rostislav Spinar \]

* iqrf-gateway-daemon (2.2.0-rc6)
* iqrf-gateway-webapp (2.0.0-rc17)
* iqrf-gateway-controller (1.0.0-rc9)

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Wed, 10 Jun 2020 20:15:00 +0200

1.0.0-rc5 testing; urgency=medium

 \[ Roman Ondráček \]
 \[ Rostislav Spinar \]

* iqrf-gateway-daemon (2.2.0-rc6)
* iqrf-gateway-webapp (2.0.0-rc15)
* iqrf-gateway-controller (1.0.0-rc9)

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Tue, 26 May 2020 22:15:00 +0200

1.0.0-rc4 testing; urgency=medium

 \[ Roman Ondráček \]
 \[ Rostislav Spinar \]

* iqrf-gateway-daemon (2.2.0-rc5)
* iqrf-gateway-webapp (2.0.0-rc15)
* iqrf-gateway-controller (1.0.0-rc9)

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Mon, 25 May 2020 23:45:00 +0200

1.0.0-rc3 testing; urgency=medium

 \[ Roman Ondráček \]
 \[ Rostislav Spinar \]

* iqrf-gateway-daemon (2.2.0-rc5)
* iqrf-gateway-webapp (2.0.0-rc14)
* iqrf-gateway-controller (1.0.0-rc8)

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Fri, 22 May 2020 02:45:00 +0200

1.0.0-rc2 testing; urgency=medium

 \[ Roman Ondráček \]
 \[ Rostislav Spinar \]

* iqrf-gateway-daemon (2.2.0-rc5)
* iqrf-gateway-webapp (2.0.0-rc13)
* iqrf-gateway-controller (1.0.0-rc6)

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Mon, 11 May 2020 14:00:00 +0100

1.0.0-rc1 testing; urgency=medium

 \[ Roman Ondráček \]
 \[ Rostislav Spinar \]

* iqrf-gateway-daemon (2.2.0-rc4)
* iqrf-gateway-webapp (2.0.0-rc6)
* iqrf-gateway-controller (1.0.0-rc2)

-- Rostislav Spinar <rostislav.spinar@iqrf.com>  Thu, 19 Mar 2020 15:00:00 +0100

## Known issues

| Armbian Linux kernel 5.9.x breaks timing of IQRF SPI protocol and that results in IQRF GW
  daemon not being able to communicate with IQRF TR module on the gateway. This situation can
  occur e.g. when Armbian Linux auto upgrades are enabled. Therefore latest gateway image v1.2.0
  disables Linux auto upgrades completely. Previous gateway images suffer from this problem. To
  remedy this situation follow these steps:

```bash
# Downgrade Linux kernel to 5.8.x
sudo apt-get update
sudo apt-get -y --allow-downgrades install linux-image-current-sunxi=20.08.14 linux-dtb-current-sunxi=20.08.14

# Disable Auto upgrades
sudo nano /etc/apt/apt.conf.d/20auto-upgrades (APT::Periodic::Enable "0";)
 ```

| IQRF GW webapp before version 2.0.1 is vulnerable to an code injection attack by passing
  specially formed parameters to URL that is leading to RCE (CVE-2020-15227
  (<https://blog.nette.org/en/cve-2020-15227-potential-remote-code-execution-vulnerability>,
   <https://nvd.nist.gov/vuln/detail/CVE-2020-15227>)).
