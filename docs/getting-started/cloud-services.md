# Cloud services

## Cloud platforms

Prepared wizards to connect selected **cloud platforms** such as IBM cloud, MS
Azure or Amazon AWS are ready. The connection to the clouds is done using
*MQTT client*.

The wizards configure MQTT client in IQRF Gateway daemon for the selected cloud.
There is also a link to detailed PDF step by step document and the video on the
page.

![IQRF Gateway Webapp screenshot](/images/webapp-cloudibm-page.png)

## Device management

Gateway backup and restore allows you to download all required user made
configurations in a form of ZIP file.
Backup file is later used to configure the new gateway (replaced) in an easy
manner.
No IQRF backup and restore is done at this moment so IQRF maintenance must be
handled by the user separately.

![IQRF Gateway Webapp screenshot](/images/webapp-gwbackuprestore.png)

[Mender](https://mender.io/) update platform allows to register
**multiple gateways** and perform OTA as well as troubleshooting tasks such as
remote terminal and port forwarding across single or more gateways.
There is a need to register the gateway to your user account.
It is done via Mender token.

[M/Monit](https://mmonit.com//) can monitor and manage distributed computer
systems, conduct automatic maintenance and repair and execute meaningful causal
actions in error situations.
