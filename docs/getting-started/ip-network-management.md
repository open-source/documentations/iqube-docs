# IP network management

## Network connections

It is possible to configure IP setting for Ethernet and WiFi connections. It is required to use
USB WiFi dongle.

- Ethernet connection

![IQRF Gateway Webapp screenshot](/images/webapp-ipethcon.png)

- WiFi connection

![IQRF Gateway Webapp screenshot](/images/webapp-ipwirelesscon.png)

## Wiregurd VPN

[Wireguard](https://www.wireguard.com/quickstart) gateway client configuration.
