# Introduction

IQD-GW-01/02 (IQube) is a generic gateway consisting of the
[TR-76D](https://iqrf.org/products/transceivers/tr-76d) module as a DPA
coordinator, a single-board, a pre-configured SD-card and a protective plastic
housing.

This device enables IQRF® customers to demonstrate and evaluate IQRF® features,
and can be easily connected to existing servers (e.g. IBM Cloud, Microsoft Azure,
AWS IoT).
The source code of [IQRF Gateway daemon](https://gitlab.iqrf.org/open-source/iqrf-gateway-daemon)
and [IQRF Gateway webapp](https://gitlab.iqrf.org/open-source/iqrf-gateway-webapp)
is provided by the open source GitLab projects.

The device can be delivered with [Armbian](https://www.armbian.com/) based Linux
image or with [Yocto](https://www.yoctoproject.org/) based Linux image.
The decision is made by the customer.
The key features for both types of images follow.

## Armbian

- SD card filesystem is mounted as read-write
- Logs are kept in memory and rotated to SD card periodically
- Support for WireGuard VPN connection
- Support for M/Monit monitoring server
- SW updates are handled via Deb packages
- No fleet updates are available
- The customers are free to add any Debian based packages

## Yocto

- SD card filesystem is mounted as read-only with defined overlay
- Logs are kept in memory and can be rotated to SD card periodically
- Support for WireGuard VPN connection
- SW updates are handled via Mender artifacts
- Fleet updates are available via Mender server and artifacts
- Built in support for M/Monit monitoring server
- The custom applications must be built with support of our Yocto gateway SDK
- A solution based on the customer's requirements possible

## Parts

- [Orange Pi Zero](http://www.orangepi.org/html/hardWare/computerAndMicrocontrollers/details/Orange-Pi-Zero-LTS.html)
  H2 Quad Core Open Source 512MB Single Board Computer
- IQRF TR-76D extension board 01/02
- White protective plastic box
- Kingston 16GB SD card
- Power supply 2A at 5V with micro USB connector

## USB HOST port

For your awareness eventual short circuit on USB socket can cause overheating
of the device.

## SD card

Do not remove the SD card when the device is in operation!
Use [Gateway button](/getting-started/gateway-information#button) to power-off
the device first,
wait 30s to shutdown the device completely, unplug the power supply and then
remove the SD card safely.

## Technical data

- Operating Voltage: 5 V
- RF Output Power (max.): 10 dBm
- Operating Temperature: -10 \~ 40 ℃
- Interfaces: micro USB
- RJ45 Ethernet
- USB (1x)
- Dimension (LxWxH): 50 x 54 x 36 mm
- Coordinator: Contains DPA-UART plugin
