# OTA gateway updates

Update your **IQD-GW-02 (IQUBE) fleet** using [Mender](https://www.mender.io/)
platform in a few minutes.

**Open-source** [Mender server](https://mender.io/plans/faq) is freely available.
Mender also provides for a **remote access** to the device such as remote terminal
and port forwarding.

Possible image updates are:

- single file
- single directory
- multiple directories
- full root filesystem

![Mender open-source server](/images/mender-ota.png)

## Mender server

Mender server setup and operation is not provided as a service. It is up to each
user to decide about the server solution.

## IQube devices

The embedded Linux image based on the
[Yocto project](https://www.yoctoproject.org/) is **optional** for the device.
It features same basic set of the software for the IQRF and IP connectivity as
Armbian image, plus support for [Mender](https://www.mender.io/) OTA **updates**.
The SD card is mounted as **read-only** which is also important attribute.
The image can be extended based on the specific customer requirements.

[Armbian based](https://www.armbian.com/) images now also include Mender clients and
therefore Mender can be used as a remote management tool. It is possible to get access
to remote terminal for each connected device. Port forwarding is also Mender made ready
for Linux based clients, not yet for Windows.
