import { defineConfig } from 'vitepress';

export default defineConfig({
	lang: 'en_US',
	title: 'IQD-GW02 (IQUBE) documentation',
	description: 'Documentation for General IQRF gateway IQD-GW02 (IQUBE)',
	base: '/iqube/',
	head: [
		['meta', { name: 'theme-color', content: '#367fa9' }],
		['link', { rel: 'apple-touch-icon', href: '/iqube/apple-touch-icon.png', sizes: '180x180' }],
		['link', { rel: 'icon', href: '/iqube/favicon.svg', type: 'image/svg+xml' }],
		['link', { rel: 'icon', type: 'image/png', href: '/iqube/favicon-32x32.png', sizes: '32x32' }],
		['link', { rel: 'icon', type: 'image/png', href: '/iqube/favicon-16x16.png', sizes: '16x16' }],
		['link', { rel: 'alternate icon', href: '/iqube/favicon.ico' }],
		['link', { rel: 'mask-icon', href: '/iqube/favicon.svg', color: '#367fa9' }],
		['link', { rel: 'mask-icon', href: '/iqube/safari-pinned-tab.svg', color: '#367fa9' }],
	],
	outDir: '../dist/',
	lastUpdated: true,
	themeConfig: {
		editLink: {
			pattern: 'https://gitlab.iqrf.org/open-source/iqube-docs/-/blob/master/docs/:path',
		},
		logo: '/logo.svg',
		nav: [
			{
				text: 'Docs',
				link: '/introduction',
				activeMatch: '/*',
			},
			{
				text: 'Product page',
				link: 'https://www.iqrf.org/product-detail/iqd-gw-02-iqube',
				target: '_blank',
			},
			{
				text: 'E-shop',
				link: 'https://eshop.iqrf.org/p/iqube',
				target: '_blank',
			},
			{
				text: 'Other docs',
				items: [
					{
						text: 'Generic gateway',
						link: 'https://docs.iqrf.org/iqrf-gateway/',
					},
					{
						text: 'Industrial gateway',
						link: 'https://docs.iqrf.org/industrial/',
					},
				],
			},
		],
		search: {
			provider: 'local',
		},
		siteTitle: false,
		sidebar: [
			{
				items: [
					{ text: 'News', link: '/news' },
					{ text: 'Introduction', link: '/introduction' },
					{ text: 'Device services', link: '/services' },
				],
			},
			{
				text: 'Getting started',
				items: [
					{ text: 'Fist steps', link: '/getting-started/first-steps' },
					{ text: 'Gateway information', link: '/getting-started/gateway-information' },
					{ text: 'Gateway configuration', link: '/getting-started/gateway-configuration' },
					{ text: 'IQRF network management', link: '/getting-started/iqrf-network-management' },
					{ text: 'IP network management', link: '/getting-started/ip-network-management' },
					{ text: 'Cloud services', link: '/getting-started/cloud-services' },
					{ text: 'Users and API keys', link: '/getting-started/users-and-api-keys' },
				],
			},
			{
				items: [
					{ text: 'Tutorials', link: '/tutorials' },
					{ text: 'Gateway SDK', link: '/sdk' },
					{ text: 'OTA updates', link: '/ota-updates' },
					{ text: 'Advanced settings', link: '/advanced-settings' },
				],
			},
		],
	},
	vite: {
		css: {
			preprocessorOptions: {
				scss: {
					api: 'modern-compiler',
				},
			},
		},
	},
});
